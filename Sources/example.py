#!/usr/bin/env python

import sys
import random
import os
import time
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np

from domain import PredatorPreyDomain

try:
    columns, _ = os.get_terminal_size(1)
except:
    columns = 80

experiment_name = ''.join(__file__.split('.')[:-1])

if not os.path.exists('experiments'):
    os.mkdir('experiments')


data_path = 'experiments/' + experiment_name + '/'

if not os.path.exists(data_path):
    os.mkdir(data_path)

headless = len(sys.argv) == 2 and sys.argv[1] == 'h'

max_timesteps = 100000
worldsize = 4
numPredators = 0
numPreys = 1
numTrees = 1

network_learning_rate = 0.001
epsilon_max      = 1.0
epsilon_decay    = 0.9999
epsilon_increase = 0.99995
activationFunction = torch.tanh
convActivationFunction = nn.ReLU()
choosingProb = 1.0

numEpisodes = 15

numOutputs = 4
numInputs = 99 + 1

class Progress:
    def __init__(self, length):
        self.start = time.clock_gettime(0)
        self.length = length

    def update(self, status):
        now = time.clock_gettime(0)
        timediff = now - self.start
        if status != 0:
            eta = int(timediff / status - timediff)
        else:
            eta = 0
        barstring = str(int(status * 100)).rjust(3) + '%  [' + '#' * int(status * self.length) \
                + '-' * (self.length - int(status * self.length)) + ']'
        print('\r'+barstring+' '+getTimeString(eta), end='')

    @staticmethod
    def end():
        print()


class Memory:
    def __init__(self):
        self.memory = []
    def push(self, inputs, outputs, health):
        self.memory.append((inputs, outputs, health))
    def __len__(self):
        return len(self.memory)
    def __getitem__(self, key):
        return self.memory[key]
    def clear(self):
        self.memory = []

def normalize(number, mean, deviation):
    return np.tanh((number - mean) / deviation)

class Agent():
    def __init__(self, creature, index):
        self.name = ('Prey_' if creature.vegan else 'Pred_') + str(index)
        self.network = Network()
        self.network.eval()
        self.networkOptimizer = optim.Adam(self.network.parameters(), lr = network_learning_rate)
        self.memory = Memory()
        self.creature = creature
        self.criterion = nn.MSELoss()
        self.epsilon = epsilon_max
        self.samples = 0
        self.mean_reward = 160
        self.desire = 1.0
    def step(self):
        if headless:
            if self.creature.died:
                #log timealive into file
                log_file = open(data_path + a.name + '_log.lg', "a")
                log_file.write(str(len(self.memory)) + '\n')
                log_file.close()

                self.learn()
                self.memory.clear()
                self.desire = 1.0 #strive for more reward

        leftEye = torch.Tensor(self.creature.leftEye)
        rightEye = torch.Tensor(self.creature.rightEye)
        otherInputs = torch.Tensor([self.creature.health, self.creature.noise, self.creature.mouth])

        desire = torch.Tensor([self.desire])
        #calculate outputs
        if random.random() < self.epsilon:
            outputs = np.random.random(4)
        else:
            outputs = self.network(leftEye, rightEye, otherInputs, desire).detach().numpy()
        self.creature.leftLeg  = outputs[0]
        self.creature.rightLeg = outputs[1]
        self.creature.bite     = outputs[2]
        self.creature.green    = outputs[3]

        if headless:
            self.memory.push((leftEye, rightEye, otherInputs), outputs, self.creature.health)

    def learn(self):
        if not headless:
            return
        self.network.train()

        timealive = len(self.memory)

        for k in range(timealive):
            reward = normalize(k, 167, 167)
            output = self.network(self.memory[k][0][0], self.memory[k][0][1], \
                    self.memory[k][0][2], torch.Tensor([reward]))
            target = torch.Tensor(self.memory[k][1])

            self.networkOptimizer.zero_grad()
            loss = self.criterion(output, target)
            loss.backward()
            self.networkOptimizer.step()

            self.samples += 1
        if timealive > 167:
            self.epsilon = min(1.0, self.epsilon * epsilon_decay ** (timealive - 167))
        else:
            self.epsilon = min(1.0, self.epsilon * epsilon_increase ** (timealive - 167))


        #training the network
        self.network.eval()
        #for name, param in self.network.named_parameters():
        #    if param.requires_grad:
        #        print (name, param.data)

class Network(nn.Module):
    def __init__(self):
        super(Network, self).__init__()

        self.conv1 = nn.Conv1d(3, 8, kernel_size = 3, stride = 1, padding = 0)
        self.bn1   = nn.BatchNorm1d(8)
        self.conv2 = nn.Conv1d(8, 8, kernel_size = 5, stride = 1, padding = 0)
        self.bn2   = nn.BatchNorm1d(8)
        self.fc1   = nn.Linear(164, 16)
        self.fc2   = nn.Linear(16, numOutputs)

        #note for eval: was 0.0001
        torch.nn.init.normal_(self.conv1.weight.data, mean=0.0, std=0.001)
        torch.nn.init.normal_(self.conv1.bias.data, mean=0.0, std=0.001)
        torch.nn.init.normal_(self.conv2.weight.data, mean=0.0, std=0.001)
        torch.nn.init.normal_(self.conv2.bias.data, mean=0.0, std=0.001)
        torch.nn.init.normal_(self.fc1.weight.data, mean=0.0, std=0.001)
        torch.nn.init.normal_(self.fc1.bias.data, mean=0.0, std=0.001)
        torch.nn.init.normal_(self.fc2.weight.data, mean=0.0, std=0.001)
        torch.nn.init.normal_(self.fc2.bias.data, mean=0.0, std=0.001)

    def forward(self, leftEye, rightEye, otherInputs, desire):
        leftEye = convActivationFunction(self.bn1(self.conv1(leftEye)))
        leftEye = convActivationFunction(self.bn2(self.conv2(leftEye))).view(80)
        rightEye = convActivationFunction(self.bn1(self.conv1(rightEye)))
        rightEye = convActivationFunction(self.bn2(self.conv2(rightEye))).view(80)

        combined = torch.cat((leftEye,rightEye, otherInputs, desire), 0)
        combined = activationFunction(self.fc1(combined))
        combined = torch.sigmoid(self.fc2(combined))

        return combined

    @staticmethod
    def num_flat_features(x):
        size = x.size()[1:]
        num = 1
        for j in size:
            num *= j
        return num

def getTimeString(seconds):
    hours = int(seconds / 3600)
    minutes = int(seconds % 3600 / 60)
    seconds = int(seconds % 60)

    return str(hours) + ':' + str(minutes).zfill(2) + ':' + str(seconds).zfill(2)


p              = PredatorPreyDomain()
p.worldsize    = worldsize
p.numPredators = numPredators
p.numPreys     = numPreys
p.numTrees     = numTrees
p.headless     = headless
p.numReceptors = 16
p.windowName   = experiment_name

agents = []
for i in range(len(p.predators)):
    agents.append(Agent(p.predators[i], i))
for i in range(len(p.preys)):
    agents.append(Agent(p.preys[i], i))

if os.path.isfile(data_path + agents[0].name + '_network.pt'):
    print(experiment_name, '- Loading networks... ', end='')
    for a in agents:
        a.network = torch.load(data_path + a.name + '_network.pt')
        epsilon_file = open(data_path + a.name + '_epsilon.pt', "r")
        a.epsilon = float(epsilon_file.read())
        if not headless:
            a.epsilon = 0.
        epsilon_file.close()
        reward_file = open(data_path + a.name + '_reward.mr', "r")
        a.mean_reward = float(reward_file.read())
        reward_file.close()
    print('Done')
else:
    print(experiment_name, "- No trained networks found, starting new...")

for a in agents:
    if not headless:
        a.epsilon = 0.

p.start()
progress = Progress(columns - 16)
counter = 0
while True:
    counter += 1
    if headless and counter % 100 == 0:
        progress.update((counter + 1) / max_timesteps)
    if (headless and counter % 1000 == 0):
        for a in agents:
            torch.save(a.network, data_path + a.name + '_network.pt')
            epsilon_file = open(data_path + a.name + '_epsilon.pt', "w")
            epsilon_file.write(str(a.epsilon))
            epsilon_file.close()
            reward_file = open(data_path + a.name + '_reward.mr', "w")
            reward_file.write(str(a.mean_reward))
            reward_file.close()
    if (headless and counter > max_timesteps):
        progress.end()
        print(experiment_name,"- Reached", max_timesteps, "timesteps, equaling", \
                getTimeString(max_timesteps / 10))
        for a in agents:
            sample_file = open(data_path + a.name + '_samples.sp', "w")
            sample_file.write(str(a.samples))
            sample_file.close()
            print(experiment_name, "- Number of samples:", a.name + ':', a.samples)
        break

    p.step()
    for a in agents:
        a.step()
    p.cont()

for a in agents:
    a.timesdied = 0
    a.epsilon = 0
print(experiment_name,"- Evaluation...")
progress = Progress(columns - 16)
for i in range(10000):
    progress.update((i + 1)/10000)
    p.step()
    for a in agents:
        if a.creature.died:
            a.timesdied += 1
        a.step()
    p.cont()
progress.end()
for a in agents:
    eval_file = open(data_path + a.name + '_eval.ev', "w")
    eval_file.write(str(a.timesdied))
    eval_file.close()
    print('\n' + experiment_name,"- Evaluation done... times died:", a.name + ':', \
            a.timesdied, "compared to random:41 nothing:60 forward:1")

finaleval_file = open(data_path + 'Evaluation.ev', "w")
finaleval_file.write(str(sum([a.timesdied for a in agents])))
finaleval_file.close()
