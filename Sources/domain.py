#!/usr/bin/env python

import socket 
import subprocess
import struct
import math

class Creature:
    @property
    def leftLeg(self):
        return self.__leftLeg
    @leftLeg.setter
    def leftLeg(self, var):
        if var >= 0 and var <= 1.0:
            self.__leftLeg = float(var)
        else:
            raise ValueError('leftLeg has to be >= 0 and <= 1.0')

    @property
    def rightLeg(self):
        return self.__rightLeg
    @rightLeg.setter
    def rightLeg(self, var):
        if var >= 0 and var <= 1.0:
            self.__rightLeg = float(var)
        else:
            raise ValueError('rightLeg has to be >= 0 and <= 1.0')

    @property
    def bite(self):
        return self.__bite
    @bite.setter
    def bite(self, var):
        if var >= 0 and var <= 1.0:
            self.__bite = float(var)
        else:
            raise ValueError('bite has to be >= 0 and <= 1.0')

    @property
    def green(self):
        return self.__green
    @green.setter
    def green(self, var):
        if var >= 0 and var <= 1.0:
            self.__green = float(var)
        else:
            raise ValueError('green has to be >= 0 and <= 1.0')


    def __init__(self, vegan, numReceptors):
        self.vegan        = vegan
        self.numReceptors = numReceptors

        self.recvString = '?xxx' + str(3 + numReceptors * 3 * 2) + 'f'

    def setInputs(self, data):
        pack             = struct.unpack(self.recvString, data)
        self.died        = pack[0]
        #self.active      = pack[1]

        self.allInputs   = list(pack[1:])
        leftEyeData      = list(pack[1:self.numReceptors * 3 + 1])
        self.leftEye     = [[leftEyeData[0::3], leftEyeData[1::3], leftEyeData[2::3]]]
        rightEyeData     = list(pack[self.numReceptors * 3 + 1:self.numReceptors * 3 * 2 + 1])
        self.rightEye     = [[rightEyeData[0::3], rightEyeData[1::3], rightEyeData[2::3]]]
        self.health      = pack[self.numReceptors * 3 * 2 + 1]
        self.noise       = pack[self.numReceptors * 3 * 2 + 2]
        self.mouth       = pack[self.numReceptors * 3 * 2 + 3]

    def sendData(self, conn):
        data = struct.pack('4f', self.leftLeg, self.rightLeg, self.bite, self.green)
        conn.sendall(data)


class PredatorPreyDomain:
    """Class for interfacing with PredatorPreyDomain used for reinforcement learning"""
    port = 0
    host = '127.0.0.1'
    headless = True

    predators = []
    preys = []

    sendBytes = struct.calcsize('4f')

    def __init__(self):
        """creates new socket with a random port and binds to it"""
        self.started = False

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((self.host, self.port))
        self.port = self.sock.getsockname()[1]

        self.setDefaults()

    @property
    def framesBetweenControl(self):
        return self.__framesBetweenControl
    @framesBetweenControl.setter
    def framesBetweenControl(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if isinstance(var, int) and var >= 0:
            self.__framesBetweenControl = var
        else:
            raise ValueError('framesBetweenControl has to be a whole number >= 0')
    
    @property
    def windowName(self):
        return self.__windowName
    @windowName.setter
    def windowName(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if len(bytes(var, 'ascii')) <= 64:
            self.__windowName = var
        else:
            raise ValueError('windowName''s length cannot exceed 64 bytes')
    
    @property
    def numTrees(self):
        return self.__numTrees
    @numTrees.setter
    def numTrees(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if isinstance(var, int) and var >= 0:
            self.__numTrees = var
        else:
            raise ValueError('numTrees has to be a whole number >= 0')
    
    @property
    def numPredators(self):
        return self.__numPredators
    @numPredators.setter
    def numPredators(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if isinstance(var, int) and var >= 0:
            self.__numPredators = var
            self.predators = []
            for _ in range(var):
                self.predators.append(Creature(False, self.numReceptors))
        else:
            raise ValueError('numPredators has to be a whole number >= 0')
    
    @property
    def numPreys(self):
        return self.__numPreys
    @numPreys.setter
    def numPreys(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if isinstance(var, int) and var >= 0:
            self.__numPreys = var
            self.preys = []
            for _ in range(var):
                self.preys.append(Creature(True, self.numReceptors))
        else:
            raise ValueError('numPredators has to be a whole number >= 0')
    
    @property
    def worldsize(self):
        return self.__worldsize
    @worldsize.setter
    def worldsize(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if isinstance(var, int) and var > 0:
            self.__worldsize = var
        else:
            raise ValueError('worldsize has to be a whole number > 0')
    
    @property
    def growCoolDown(self):
        return self.__growCoolDown
    @growCoolDown.setter
    def growCoolDown(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= 0:
            self.__growCoolDown = float(var)
        else:
            raise ValueError('growCoolDown has to be >= 0')
    
    @property
    def velocityDamp(self):
        return self.__velocityDamp
    @velocityDamp.setter
    def velocityDamp(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= 0:
            self.__velocityDamp = float(var)
        else:
            raise ValueError('velocityDamp has to be >= 0')
    
    @property
    def rotationDamp(self):
        return self.__rotationDamp
    @rotationDamp.setter
    def rotationDamp(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= 0:
            self.__rotationDamp = float(var)
        else:
            raise ValueError('rotationDamp has to be >= 0')
    
    @property
    def sightRange(self):
        return self.__sightRange
    @sightRange.setter
    def sightRange(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= 0:
            self.__sightRange = float(var)
        else:
            raise ValueError('sightRange has to be >= 0')
    
    @property
    def sightAngle(self):
        return self.__sightAngle
    @sightAngle.setter
    def sightAngle(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= 0 and var <= 2 * math.pi:
            self.__sightAngle = float(var)
        else:
            raise ValueError('sightAngle has to be >= 0 and <= 2 * PI')
    
    @property
    def numReceptors(self):
        return self.__numReceptors
    @numReceptors.setter
    def numReceptors(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if isinstance(var, int) and var >= 0:
            self.recvString = '???x' + str(3 + var * 3 * 2) + 'f'
            self.recvBytes = struct.calcsize(self.recvString)
            if len(self.preys) != 0 and len(self.predators) != 0:
                self.preys = []
                for _ in range(self.numPreys):
                    self.preys.append(Creature(True, var))
                self.predators = []
                for _ in range(self.numPredators):
                    self.predators.append(Creature(False, var))
            self.__numReceptors = var
        else:
            raise ValueError('numReceptors has to be a whole number >= 0')
    
    @property
    def biteCoolDown(self):
        return self.__biteCoolDown
    @biteCoolDown.setter
    def biteCoolDown(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= 0:
            self.__biteCoolDown = float(var)
        else:
            raise ValueError('biteCoolDown has to be >= 0')
    
    @property
    def lifeTime(self):
        return self.__lifeTime
    @lifeTime.setter
    def lifeTime(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= 0:
            self.__lifeTime = float(var)
        else:
            raise ValueError('lifeTime has to be >= 0')
    
    @property
    def predatorWeight(self):
        return self.__predatorWeight
    @predatorWeight.setter
    def predatorWeight(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= 0:
            self.__predatorWeight = float(var)
        else:
            raise ValueError('predatorWeight has to be >= 0')
    
    @property
    def predatorBiteForce(self):
        return self.__predatorBiteForce
    @predatorBiteForce.setter
    def predatorBiteForce(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= 0 and var <= 1.0:
            self.__predatorBiteForce = float(var)
        else:
            raise ValueError('predatorBiteForce has to be >= 0 and <= 1.0')
    
    @property
    def predatorMeatGain(self):
        return self.__predatorMeatGain
    @predatorMeatGain.setter
    def predatorMeatGain(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= -1. and var <= 1.:
            self.__predatorMeatGain = float(var)
        else:
            raise ValueError('predatorMeatGain has to be >= -1.0 and <= 1.0')
    
    @property
    def predatorFruitGain(self):
        return self.__predatorFruitGain
    @predatorFruitGain.setter
    def predatorFruitGain(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= -1. and var <= 1.:
            self.__predatorFruitGain = float(var)
        else:
            raise ValueError('predatorFruitGain has to be >= -1.0 and <= 1.0')
    
    @property
    def predatorLegPower(self):
        return self.__predatorLegPower
    @predatorLegPower.setter
    def predatorLegPower(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= 0:
            self.__predatorLegPower = float(var)
        else:
            raise ValueError('predatorLegPower has to be >= 0')
    
    @property
    def predatorRotPower(self):
        return self.__predatorRotPower
    @predatorRotPower.setter
    def predatorRotPower(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= 0:
            self.__predatorRotPower = float(var)
        else:
            raise ValueError('predatorRotPower has to be >= 0')
    
    @property
    def preyWeight(self):
        return self.__preyWeight
    @preyWeight.setter
    def preyWeight(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= 0:
            self.__preyWeight = float(var)
        else:
            raise ValueError('preyWeight has to be >= 0')
    
    @property
    def preyBiteForce(self):
        return self.__preyBiteForce
    @preyBiteForce.setter
    def preyBiteForce(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= 0 and var <= 1.0:
            self.__preyBiteForce = float(var)
        else:
            raise ValueError('preyBiteForce has to be >= 0 and <= 1.0')
    
    @property
    def preyMeatGain(self):
        return self.__preyMeatGain
    @preyMeatGain.setter
    def preyMeatGain(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= -1. and var <= 1.:
            self.__preyMeatGain = float(var)
        else:
            raise ValueError('preyMeatGain has to be >= -1.0 and <= 1.0')
    
    @property
    def preyFruitGain(self):
        return self.__preyFruitGain
    @preyFruitGain.setter
    def preyFruitGain(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= -1. and var <= 1.:
            self.__preyFruitGain = float(var)
        else:
            raise ValueError('preyFruitGain has to be >= 1.0 and <= 1.0')
    
    @property
    def preyLegPower(self):
        return self.__preyLegPower
    @preyLegPower.setter
    def preyLegPower(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= 0:
            self.__preyLegPower = float(var)
        else:
            raise ValueError('preyLegPower has to be >= 0')
    
    @property
    def preyRotPower(self):
        return self.__preyRotPower
    @preyRotPower.setter
    def preyRotPower(self, var):
        if self.started:
            raise Exception('Domain properties cannot be changed after being started')
        if var >= 0:
            self.__preyRotPower = float(var)
        else:
            raise ValueError('preyRotPower has to be >= 0')

    def setDefaults(self):
        self.framesBetweenControl = 6
        self.windowName           = 'Predator-Prey Domain'

        self.worldsize            = 4
        self.growCoolDown         = 16.666

        self.velocityDamp         = 0.95
        self.rotationDamp         = 0.95

        self.sightRange           = 512.
        self.sightAngle           = math.pi / 2 * 1.1
        self.numReceptors         = 16

        self.numTrees             = 1
        self.numPredators         = 1
        self.numPreys             = 2

        self.biteCoolDown         = 3.33
        self.lifeTime             = 33.333

        self.predatorWeight       = 50.
        self.predatorBiteForce    = 0.6
        self.predatorMeatGain     = 0.8
        self.predatorFruitGain    = 0.
        self.predatorLegPower     = 0.4
        self.predatorRotPower     = 0.02

        self.preyWeight           = 40.
        self.preyBiteForce        = 0.1
        self.preyMeatGain         = 0.1
        self.preyFruitGain        = 0.4
        self.preyLegPower         = 0.2
        self.preyRotPower         = 0.01

    def sendParameters(self):
        data = struct.pack('i64s4i5fi14f', self.framesBetweenControl, bytes(self.windowName, 'ascii'), self.numTrees, self.numPredators, self.numPreys, self.worldsize, self.growCoolDown, self.velocityDamp, self.rotationDamp, self.sightRange, self.sightAngle, self.numReceptors, self.biteCoolDown, self.lifeTime, self.predatorWeight, self.predatorBiteForce, self.predatorMeatGain, self.predatorFruitGain, self.predatorLegPower, self.predatorRotPower, self.preyWeight, self.preyBiteForce, self.preyMeatGain, self.preyFruitGain, self.preyLegPower, self.preyRotPower)
        self.conn.sendall(data)

    def info(self):
        print('''========#DOMAIN PARAMETERS#=============================

framesBetweenControl:
simulation steps performed by the domain between exchange of data

windowName:
window name of the domain when not running in headless mode


numTrees:
total number of trees

numPredators:
total number of predators

numPreys:
total number of preys


worldsize:
world-size in units of vertical and horizontal number of cells

growCoolDown:
how much time [s] a fruit takes to fully regenerate


velocityDamp:
at every simulation-step the current velocity of the creature is multiplied with this factor

rotationDamp:
at every simulation-step the current rotational velocity of the creature is multiplied with this factor


sightRange:
maximum range at which the creature's eyes perceive the environment

sightAngle:
the angle at which the creature's eyes perceive the environment

numReceptors:
total number of sensors per eye (R+G+B)


biteCoolDown:
how long [s] a creature has to wait between bites

lifeTime:
maximum length of time [s] a creature lives if it has full health and stops eating


predatorWeight:
how much a predator weighs, relevant for collisions and in conjunction with leg-power for acceleration

predatorBiteForce:
maximum amount of health that is deducted from the target if a bite of the predator connects
predatorMeatGain:

maximum amount of health a predator gains from biting another creature
predatorFruitGain:

maximum amount of health a predator gains from biting fruit
predatorLegPower:
maximum amount of velocity-gain per simulation-step a predator gains when activating one leg

predatorRotPower:
maximum amount of rotational velocity-gain per simulation-step a predator gains when activating one leg


preyWeight:
how much a prey weighs, relevant for collisions and in conjunction with leg-power for acceleration

preyBiteForce:
maximum amount of health that is deducted from the target if a bite of the prey connects

preyMeatGain:
maximum amount of health a prey gains from biting another creature

preyFruitGain:
maximum amount of health a prey gains from biting fruit

preyLegPower:
maximum amount of velocity-gain per simulation-step a prey gains when activating one leg

preyRotPower:
maximum amount of rotational velocity-gain per simulation-step a prey gains when activating one leg

========#INPUTS#========================================

leftEye / rightEye:
[<numReceptors> x 3]-matrix of visual perception of the left or right eye respectively with <numReceptors> RGB inputs, intensity inversely proportional to the distance to the target
R R R R     R R R 
G G G G ... G G G 
B B B B     B B B 

health:
current health of the creature from 0. to 1., 0. meaning dead and 1. having full health

noise:
current noise around the creature, measured in amount of velocity around the creature (including itself), proportional to the maximum achievable speed and inversely proportional to the distance to the target

mouth:
current state of the bite-cool-down, 0. meaning having just bitten, 1. meaning ready to bite


========#OUTPUTS#=======================================

leftLeg / rightLeg:
degree of activation of the left or right leg respectively, 0. meaning no activation and 1. meaning full activation

bite:
force with which the creature bites, 0. meaning no force, 1. meaning full force, cool-down is only activated if the bite connects

green:
the green value of the creature, from 0. to 1. , changing how the creature is perceived by others, predators are red, full green value would lead to a yellow creature, preys are blue, a full green value would lead to a cyan creature, useful for learning communication
''')

    def receive(self, numBytes):
        data = bytes(0)
        while len(data) < numBytes:
            more = self.conn.recv(numBytes - len(data))
            if not more:
                exit(0)
            data += more
        return data

    def start(self):
        """starts the program"""
        self.started = True
        self.sock.listen(1)
        if (self.headless):
            subprocess.Popen(["Domain/build/main", str(self.port)])
        else:
            subprocess.Popen(["Domain/build/render", str(self.port)])
            #subprocess.Popen(["/usr/bin/valgrind", "-q", "--leak-check=full", "--track-origins=yes", "--show-leak-kinds=all", "Domain/build/render_debug", str(self.port)])
        self.conn = self.sock.accept()[0]

        self.sendParameters()

    def step(self):
        """receives data from domain, returns new Creatures with new inputs"""
        try:
            for predator in self.predators:
                predator.setInputs(self.receive(self.recvBytes))
            for prey in self.preys:
                prey.setInputs(self.receive(self.recvBytes))
        except socket.error:
            print('Domain exited')
            exit(0)

    def cont (self):
        """sends data to domain, continues simulation"""
        try:
            for predator in self.predators:
                predator.sendData(self.conn)
            for prey in self.preys:
                prey.sendData(self.conn)
        except socket.error:
            print('Domain exited')
            exit(0)
