#pragma once

class Parameters
{
  private:
    Parameters()
    {
    }

  public:
    // Parameters(const Parameters &) = delete;
    // Parameters(Parameters &&)      = delete;
    // Parameters &operator=(const Parameters &) = delete;
    // Parameters &operator=(Parameters &&) = delete;

    static Parameters &getInstance();
    int framesBetweenControl;
    char windowName[65];

    int numTrees;
    int numPredators;
    int numPreys;

    float worldsize;
    float growCoolDown;

    float velocityDamp;
    float rotationDamp;

    float sightRange;
    float sightAngle;
    int numReceptors;

    float biteCoolDown;
    float lifeTime;

    float predatorWeight;
    float predatorBiteForce;
    float predatorMeatGain;
    float predatorFruitGain;
    float predatorLegPower;
    float predatorRotPower;

    float preyWeight;
    float preyBiteForce;
    float preyMeatGain;
    float preyFruitGain;
    float preyLegPower;
    float preyRotPower;
};
