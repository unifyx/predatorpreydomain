#include "creature.h"

Creature::Creature(bool _vegan, float _rotation, float *_position): parameters(Parameters::getInstance())
{
    vegan = _vegan;
    if(vegan)
    {
        size          = parameters.preyWeight;
        biteforce     = parameters.preyBiteForce;
        meatFoodGain  = parameters.preyMeatGain;
        fruitFoodGain = parameters.preyFruitGain;
        legpower      = parameters.preyLegPower;
        rotpower      = parameters.preyRotPower;
        maxspeed      = parameters.preyLegPower * 2.f / (1.f - parameters.velocityDamp);
    }
    else
    {
        size          = parameters.predatorWeight;
        biteforce     = parameters.predatorBiteForce;
        meatFoodGain  = parameters.predatorMeatGain;
        fruitFoodGain = parameters.predatorFruitGain;
        legpower      = parameters.predatorLegPower;
        rotpower      = parameters.predatorRotPower;
        maxspeed      = parameters.predatorLegPower * 2.f / (1.f - parameters.velocityDamp);
    }

    left_eye =
        static_cast<unsigned char *>(malloc(sizeof(char) * static_cast<unsigned long>(parameters.numReceptors) * 3));
    right_eye =
        static_cast<unsigned char *>(malloc(sizeof(char) * static_cast<unsigned long>(parameters.numReceptors) * 3));

    position[0] = _position[0];
    position[1] = _position[1];

    rotation = _rotation;

    green_value = 0;

    vel[0] = 0.f;
    vel[1] = 0.f;

    rotvel = 0.f;

    legstatus[0] = 0.f;
    legstatus[1] = 0.f;

    eat = 1.f;

    health = .5f;

    bite = 0.f;
    died = false;
}

void Creature::update()
{
    vel[0] *= parameters.velocityDamp;
    vel[1] *= parameters.velocityDamp;
    rotvel *= parameters.rotationDamp;

    vel[0] += (legstatus[0] + legstatus[1]) * legpower * sinf(rotation);
    vel[1] -= (legstatus[0] + legstatus[1]) * legpower * cosf(rotation);
    rotvel += legstatus[0] * rotpower;
    rotvel -= legstatus[1] * rotpower;

    rotation = fmod(rotation + rotvel + 2 * M_PIf32, 2 * M_PIf32);

    health = std::max(0.f, health - (1.f / parameters.lifeTime));
    eat    = std::min(1.f, eat + (1.f / parameters.biteCoolDown));
}
