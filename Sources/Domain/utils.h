#pragma once

#include <cmath>
#include <cstdlib>

class Utils
{
  public:
    static float randf(float limit)
    {
        return float(rand()) / float(RAND_MAX) * limit;
    }
    static float rad2deg(float rad)
    {
        return rad / 2.f / M_PIf32 * 360.f;
    }
    static bool getDistance(float *lambda, float c_x, float c_y, float dir_x, float dir_y, float eye_x, float eye_y,
                            float r)
    {
        float sqrtResult = powf((-2 * c_x * dir_x + 2 * dir_x * eye_x + 2 * dir_y * eye_y - 2 * dir_y * c_y), 2) -
                           4 * (powf(dir_x, 2) + powf(dir_y, 2)) *
                               (powf(c_x, 2) - 2 * c_x * eye_x + powf(eye_x, 2) + powf(eye_y, 2) - 2 * eye_y * c_y +
                                powf(c_y, 2) - powf(r, 2));
        if(sqrtResult < 0.f)
        {
            return false;
        }
        float one = (sqrtf(sqrtResult) + 2 * c_x * dir_x - 2 * dir_x * eye_x - 2 * dir_y * eye_y + 2 * dir_y * c_y) /
                    (2 * (powf(dir_x, 2) + powf(dir_y, 2)));
        float two = (-sqrtf(sqrtResult) + 2 * c_x * dir_x - 2 * dir_x * eye_x - 2 * dir_y * eye_y + 2 * dir_y * c_y) /
                    (2 * (powf(dir_x, 2) + powf(dir_y, 2)));
        if(one < 0.f && two < 0.f)
        {
            return false;
        }
        if(one >= 0.f && one < two)
        {
            *lambda = one;
        }
        else if(two >= 0.f)
        {
            *lambda = two;
        }
        return true;
    }
};
