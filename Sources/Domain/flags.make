COMP ?= clang++
VALG = valgrind -q --leak-check=full --track-origins=yes --show-leak-kinds=all
SUDOCHECK := @if [ "$(shell id -u)" != "0" ]; then echo "Please run this as root" ; exit 1; fi

DEBUGFLAGS = \
-O0 \
-g \
-DEBUG

CFLAGS ?= \
-ferror-limit=0 \
-Wall \
-Wextra \
-Wmost \
-Wpedantic \
-Warray-bounds-pointer-arithmetic \
-Wassign-enum \
-Watomic-implicit-seq-cst \
-Wauto-import \
-Wbad-function-cast \
-Wbitfield-enum-conversion \
-Wbitwise-op-parentheses \
-Wcast-align \
-Wcast-qual \
-Wchar-subscripts \
-Wcomma \
-Wcomment \
-Wconditional-uninitialized \
-Wconversion \
-Wcovered-switch-default \
-Wdeclaration-after-statement \
-Wdouble-promotion \
-Wduplicate-enum \
-Wduplicate-method-arg \
-Wduplicate-method-match \
-Wembedded-directive \
-Wempty-init-stmt \
-Wflexible-array-extensions \
-Wfloat-conversion \
-Wfloat-overflow-conversion \
-Wfloat-zero-conversion \
-Wformat-nonliteral \
-Wformat-pedantic \
-Wformat-security \
-Wgnu \
-Widiomatic-parentheses \
-Wignored-qualifiers \
-Wimplicit-fallthrough \
-Wimplicit-int-conversion \
-Wimplicit-float-conversion \
-Winfinite-recursion \
-Wkeyword-macro \
-Wlanguage-extension-token \
-Wlogical-op-parentheses \
-Wmain \
-Wmethod-signatures \
-Wmicrosoft \
-Wmissing-braces \
-Wmissing-method-return-type \
-Wmissing-noreturn \
-Wmissing-prototypes \
-Wmissing-variable-declarations \
-Wmove \
-Wnested-anon-types \
-Wnewline-eof \
-Wnonportable-system-include-path \
-Wnull-pointer-arithmetic \
-Wnullability-extension \
-Wnullable-to-nonnull-conversion \
-Wold-style-cast \
-Wover-aligned \
-Woverlength-strings \
-Wpacked \
-Wpessimizing-move \
-Wno-padded \
-Wpragma-pack-suspicious-include \
-Wrange-loop-analysis \
-Wredundant-move \
-Wredundant-parens \
-Wreorder \
-Wreserved-id-macro \
-Wself-move \
-Wshadow-all \
-Wshift-sign-overflow \
-Wshorten-64-to-32 \
-Wsign-compare \
-Wsign-conversion \
-Wsometimes-uninitialized \
-Wstrict-prototypes \
-Wstrict-selector-match \
-Wstring-conversion \
-Wswitch-enum \
-Wtautological-compare \
-Wtautological-constant-in-range-compare \
-Wtautological-overlap-compare \
-Wtautological-type-limit-compare \
-Wtautological-unsigned-enum-zero-compare \
-Wtautological-unsigned-zero-compare \
-Wthread-safety-analysis \
-Wthread-safety-attributes \
-Wthread-safety-beta \
-Wthread-safety-negative \
-Wthread-safety-precise \
-Wthread-safety-reference \
-Wthread-safety-verbose \
-Wtrigraphs \
-Wundeclared-selector \
-Wundef \
-Wundefined-func-template \
-Wundefined-internal-type \
-Wundefined-reinterpret-cast \
-Wuninitialized \
-Wunneeded-internal-declaration \
-Wunneeded-member-function \
-Wunreachable-code-aggressive \
-Wunreachable-code-loop-increment \
-Wunused \
-Wunused-variable \
-Wunused-label \
-Wunused-lambda-capture \
-Wunused-local-typedef \
-Wno-unused-parameter \
-Wused-but-marked-unused \
-Wvla \
-Wzero-as-null-pointer-constant \
-Werror \
-Wno-unused-macros \
-Wno-gnu-zero-variadic-macro-arguments \
-Wno-gnu-statement-expression

FASTFLAGS = \
-Ofast

OPTREPORT = \
"-Rpass=.*" \
"-Rpass-missed=.*" \
"-Rpass-analysis=.*"
