#pragma once

#include "parameters.h"

#include <algorithm>
#include <cmath>

#define TREESIZE 200.f
#define FRUITSIZE TREESIZE / 5.f

#define TREE_R 100
#define TREE_G 100
#define TREE_B 0
#define FRUIT_R 200
#define FRUIT_G 50
#define FRUIT_B 200

class Fruit
{
  public:
    float status;
    float position[2];

    Fruit();
};

class Tree
{
  public:
    float position[2];
    float rotation;
    Fruit fruits[8];

    Tree(float *_pos, float _rot);
    void update();

    Parameters &parameters;
};
