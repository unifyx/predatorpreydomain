#include "grid.h"

#include <arpa/inet.h>
#include <cmath>
#include <iostream>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

class Main
{
  public:
    [[noreturn]] void startLoop();

    Main(int);

  private:
    void establishConnection(unsigned short);

    unsigned long currentFrame;
    Grid *grid;
    int sock;
    float buffer[sizeof(float) * 4];

    void receiveParameters();
    Parameters &parameters;
};
