#include "render.h"

Render::Render(int port): parameters(Parameters::getInstance())
{
    currentFrame     = 0;
    selectedCreature = nullptr;
    currentZoom      = 1.f;
    establishConnection(static_cast<unsigned short>(port));
    receiveParameters();
    grid = new Grid();
    initRenderWindow(500, 500);
    initResources();
}

void Render::receiveParameters()
{
    read(sock, &parameters.framesBetweenControl, sizeof(int));
    read(sock, &parameters.windowName, sizeof(char) * 64);
    parameters.windowName[64] = '\0';

    read(sock, &parameters.numTrees, sizeof(int));
    read(sock, &parameters.numPredators, sizeof(int));
    read(sock, &parameters.numPreys, sizeof(int));

    int worldsize_temp;
    read(sock, &worldsize_temp, sizeof(int));
    parameters.worldsize = CELLSIZE * float(worldsize_temp);
    read(sock, &parameters.growCoolDown, sizeof(float));
    parameters.growCoolDown *= 60.f;

    read(sock, &parameters.velocityDamp, sizeof(float));
    read(sock, &parameters.rotationDamp, sizeof(float));

    read(sock, &parameters.sightRange, sizeof(float));
    read(sock, &parameters.sightAngle, sizeof(float));
    read(sock, &parameters.numReceptors, sizeof(int));

    read(sock, &parameters.biteCoolDown, sizeof(float));
    parameters.biteCoolDown *= 60.f;
    read(sock, &parameters.lifeTime, sizeof(float));
    parameters.lifeTime *= 60.f;

    read(sock, &parameters.predatorWeight, sizeof(float));
    read(sock, &parameters.predatorBiteForce, sizeof(float));
    read(sock, &parameters.predatorMeatGain, sizeof(float));
    read(sock, &parameters.predatorFruitGain, sizeof(float));
    read(sock, &parameters.predatorLegPower, sizeof(float));
    read(sock, &parameters.predatorRotPower, sizeof(float));

    read(sock, &parameters.preyWeight, sizeof(float));
    read(sock, &parameters.preyBiteForce, sizeof(float));
    read(sock, &parameters.preyMeatGain, sizeof(float));
    read(sock, &parameters.preyFruitGain, sizeof(float));
    read(sock, &parameters.preyLegPower, sizeof(float));
    read(sock, &parameters.preyRotPower, sizeof(float));
}

void Render::establishConnection(unsigned short port)
{
    sock = 0;
    struct sockaddr_in serv_addr;
    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        std::cerr << "Socket creation error" << std::endl;
        exit(EXIT_FAILURE);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port   = htons(port);

    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        std::cerr << "Invalid address/ Address not supported" << std::endl;
        exit(EXIT_FAILURE);
    }

    if(connect(sock, reinterpret_cast<struct sockaddr *>(&serv_addr), sizeof(serv_addr)) < 0)
    {
        std::cerr << "Connection Failed" << std::endl;
        exit(EXIT_FAILURE);
    }
}

void Render::initRenderWindow(unsigned int width, unsigned int height)
{
    window.create(sf::VideoMode(width, height), parameters.windowName);
    window.setFramerateLimit(60);

    view = window.getDefaultView();
}
void Render::initResources()
{
    texCreatureBody.loadFromFile("Domain/graphics/creature_body.png");
    texCreatureBody.setSmooth(true);
    spCreatureBody.setTexture(texCreatureBody);
    spCreatureBody.setOrigin(sf::Vector2f(82.f, 98.f));

    texCreatureLeftFoot.loadFromFile("Domain/graphics/creature_leftfoot.png");
    texCreatureLeftFoot.setSmooth(true);
    spCreatureLeftFoot.setTexture(texCreatureLeftFoot);
    spCreatureLeftFoot.setOrigin(sf::Vector2f(82.f, 98.f));

    texCreatureRightFoot.loadFromFile("Domain/graphics/creature_rightfoot.png");
    texCreatureRightFoot.setSmooth(true);
    spCreatureRightFoot.setTexture(texCreatureRightFoot);
    spCreatureRightFoot.setOrigin(sf::Vector2f(82.f, 98.f));

    texCreatureMouth.loadFromFile("Domain/graphics/creature_mouth.png");
    texCreatureMouth.setSmooth(true);
    spCreatureMouth.setTexture(texCreatureMouth);
    spCreatureMouth.setOrigin(sf::Vector2f(82.f, 98.f));

    texTree.loadFromFile("Domain/graphics/tree.png");
    texTree.setSmooth(true);
    spTree.setTexture(texTree);
    spTree.setOrigin(sf::Vector2f(602.f, 602.f));
    spTree.setScale(TREESIZE / 1204.f, TREESIZE / 1204.f);

    texFruit.loadFromFile("Domain/graphics/fruit.png");
    texFruit.setSmooth(true);
    spFruit.setTexture(texFruit);
    spFruit.setOrigin(sf::Vector2f(62.f, 62.f));
    spFruit.setScale(FRUITSIZE / 125.f, FRUITSIZE / 125.f);
}

void Render::zoom(float factor, int x, int y)
{
    float zoom_factor = powf(1.1f, factor);
    currentZoom *= zoom_factor;
    sf::Vector2f beforeCenter = window.mapPixelToCoords(sf::Vector2i(x, y));

    view.zoom(zoom_factor);
    window.setView(view);
    sf::Vector2f afterCenter = window.mapPixelToCoords(sf::Vector2i(x, y));

    view.move(beforeCenter - afterCenter);
    window.setView(view);
}

void Render::startLoop()
{
    bool recording = false;
    // Start the game loop
    while(window.isOpen())
    {
        currentFrame++;
        // Process events
        sf::Event event;
        while(window.pollEvent(event))
        {
            // Close window: exit
            if(event.type == sf::Event::Closed)
            {
                window.close();
            }

            if(event.type == sf::Event::KeyPressed)
            {
                // Escape or Q pressed: exit
                if(event.key.code == sf::Keyboard::Escape || event.key.code == sf::Keyboard::Q)
                {
                    window.close();
                }
                // R: toggle recording
                if(event.key.code == sf::Keyboard::R)
                {
                    recording = !recording;
                }
            }
            if(event.type == sf::Event::MouseButtonPressed)
            {
                if(event.mouseButton.button == sf::Mouse::Left)
                {
                    sf::Vector2f coordinates =
                        window.mapPixelToCoords(sf::Vector2i(event.mouseButton.x, event.mouseButton.y));
                    selectedCreature = nullptr;
                    for(unsigned long x = 0; x < grid->numCells; x++)
                    {
                        for(unsigned long y = 0; y < grid->numCells; y++)
                        {
                            GridCell *curCell = &grid->cells[x * grid->numCells + y];
                            for(unsigned long n = 0; n < curCell->creatures.size(); n++)
                            {
                                Creature *curCreature = curCell->creatures[n];
                                float pos_x           = curCreature->position[0] + float(x) * CELLSIZE;
                                float pos_y           = curCreature->position[1] + float(y) * CELLSIZE;
                                if(powf(pos_x - float(coordinates.x), 2.f) + powf(pos_y - float(coordinates.y), 2.f) <
                                   powf(curCreature->size, 2) / 4)
                                {
                                    selectedCreature = curCreature;
                                }
                            }
                        }
                    }
                }
            }
            if(event.type == sf::Event::MouseWheelScrolled)
            {
                float delta = event.mouseWheelScroll.delta * -1;
                zoom(delta, event.mouseWheelScroll.x, event.mouseWheelScroll.y);
            }
            if(event.type == sf::Event::Resized)
            {
                view.setSize(float(event.size.width), float(event.size.height));
                currentZoom = 1.f;
                window.setView(view);
            }
        }
        // Clear screen
        window.clear(background);

        for(unsigned long x = 0; x < grid->numCells; x++)
        {
            for(unsigned long y = 0; y < grid->numCells; y++)
            {
                GridCell *curCell = &grid->cells[x * grid->numCells + y];
                for(auto const &i: curCell->trees)
                {
                    renderTree(i, x, y);
                }
                for(auto const &i: curCell->creatures)
                {
                    renderCreature(i, x, y);
                }
            }
        }
        sf::VertexArray lines(sf::Lines, 8);
        lines[0].position = sf::Vector2f(0.f, 0.f);
        lines[1].position = sf::Vector2f(parameters.worldsize, 0.f);
        lines[2].position = sf::Vector2f(0.f, 0.f);
        lines[3].position = sf::Vector2f(0.f, parameters.worldsize);

        lines[4].position = sf::Vector2f(parameters.worldsize, parameters.worldsize);
        lines[5].position = sf::Vector2f(parameters.worldsize, 0.f);
        lines[6].position = sf::Vector2f(parameters.worldsize, parameters.worldsize);
        lines[7].position = sf::Vector2f(0.f, parameters.worldsize);
        window.draw(lines);

        // Draw the inputs
        if(selectedCreature != nullptr)
        {
            float pos_x = 0;
            float pos_y = 0;
            for(unsigned long x = 0; x < grid->numCells; x++)
            {
                for(unsigned long y = 0; y < grid->numCells; y++)
                {
                    GridCell *curCell = &grid->cells[x * grid->numCells + y];
                    for(auto const &i: curCell->creatures)
                    {
                        if(i == selectedCreature)
                        {
                            pos_x = selectedCreature->position[0] + float(x) * CELLSIZE;
                            pos_y = selectedCreature->position[1] + float(y) * CELLSIZE;
                        }
                    }
                }
            }
            // draw lines
            if(selectedCreature->size / 2 < 75.f * currentZoom)
            {
                sf::VertexArray uiline(sf::Lines, 4);
                uiline[0].position =
                    sf::Vector2f(pos_x - selectedCreature->size / 2, pos_y - selectedCreature->size / 2);
                uiline[1].position = sf::Vector2f(pos_x - 75.f * currentZoom, pos_y - 75.f * currentZoom);
                uiline[2].position = sf::Vector2f(pos_x - 75.f * currentZoom, pos_y - 75.f * currentZoom);
                uiline[3].position = sf::Vector2f(pos_x - 240.f * currentZoom, pos_y - 75.f * currentZoom);
                window.draw(uiline);
            }

            // draw eyes
            for(int i = 0; i < parameters.numReceptors; i++)
            {
                sf::RectangleShape sensor(sf::Vector2f(10.f * currentZoom, 10.f * currentZoom));
                sensor.setOutlineThickness(1.f * currentZoom);
                sensor.setOutlineColor(sf::Color::White);
                sensor.setFillColor(sf::Color(selectedCreature->left_eye[i * 3], selectedCreature->left_eye[i * 3 + 1],
                                              selectedCreature->left_eye[i * 3 + 2]));
                sensor.setPosition(pos_x - 240.f * currentZoom + 15.f * float(i) * currentZoom,
                                   pos_y - 115.f * currentZoom);

                sf::RectangleShape sensor2(sf::Vector2f(10.f * currentZoom, 10.f * currentZoom));
                sensor2.setOutlineThickness(1.f * currentZoom);
                sensor2.setOutlineColor(sf::Color::White);
                sensor2.setFillColor(sf::Color(selectedCreature->right_eye[i * 3],
                                               selectedCreature->right_eye[i * 3 + 1],
                                               selectedCreature->right_eye[i * 3 + 2]));
                sensor2.setPosition(pos_x - 240.f * currentZoom + 15.f * float(i) * currentZoom,
                                    pos_y - 95.f * currentZoom);
                window.draw(sensor);
                window.draw(sensor2);
            }

            unsigned char noiseVal  = static_cast<unsigned char>(selectedCreature->noise * 255.f);
            unsigned char healthVal = static_cast<unsigned char>(selectedCreature->health * 255.f);
            unsigned char eatVal    = static_cast<unsigned char>(selectedCreature->eat * 255.f);

            sf::RectangleShape noise(sf::Vector2f(10.f * currentZoom, 10.f * currentZoom));
            noise.setOutlineThickness(1.f * currentZoom);
            noise.setOutlineColor(sf::Color::White);
            noise.setFillColor(sf::Color(noiseVal, noiseVal, noiseVal));
            noise.setPosition(pos_x - 240.f * currentZoom + 15.f * (float(parameters.numReceptors) + 1.f) * currentZoom,
                              pos_y - 105.f * currentZoom);

            sf::RectangleShape health(sf::Vector2f(10.f * currentZoom, 10.f * currentZoom));
            health.setOutlineThickness(1.f * currentZoom);
            health.setOutlineColor(sf::Color::White);
            health.setFillColor(sf::Color(healthVal, healthVal, healthVal));
            health.setPosition(pos_x - 240.f * currentZoom +
                                   15.f * (float(parameters.numReceptors) + 2.f) * currentZoom,
                               pos_y - 105.f * currentZoom);

            sf::RectangleShape eat(sf::Vector2f(10.f * currentZoom, 10.f * currentZoom));
            eat.setOutlineThickness(1.f * currentZoom);
            eat.setOutlineColor(sf::Color::White);
            eat.setFillColor(sf::Color(eatVal, eatVal, eatVal));
            eat.setPosition(pos_x - 240.f * currentZoom + 15.f * (float(parameters.numReceptors) + 3.f) * currentZoom,
                            pos_y - 105.f * currentZoom);

            window.draw(noise);
            window.draw(eat);
            window.draw(health);
        }

        // Update the window
        window.display();
        if(recording)
        {
            sf::Texture texture;
            texture.create(window.getSize().x, window.getSize().y);
            texture.update(window);
            std::string imageName = "00000000";
            std::string number    = std::to_string(currentFrame);
            imageName.replace(imageName.length() - number.length(), number.length(), number);
            texture.copyToImage().saveToFile("/tmp/" + imageName + ".png");
        }

        grid->updatePhysics();
        if(currentFrame % static_cast<unsigned long>(parameters.framesBetweenControl) == 0)
        {
            grid->updateInputs();
            // send to python

            for(Creature *creature: grid->allCreatures)
            {
                const char padding = 0;
                send(sock, &(creature->died), sizeof(creature->died), 0);
                send(sock, &padding, sizeof(padding), 0);
                send(sock, &padding, sizeof(padding), 0);
                send(sock, &padding, sizeof(padding), 0);
                for(int i = 0; i < parameters.numReceptors * 3; i++)
                {
                    float value = float(creature->left_eye[i]) / 255.f;
                    send(sock, &value, sizeof(float), 0);
                }
                for(int i = 0; i < parameters.numReceptors * 3; i++)
                {
                    float value = float(creature->right_eye[i]) / 255.f;
                    send(sock, &value, sizeof(float), 0);
                }
                float health = creature->health;
                float noise  = creature->noise;
                float eat    = creature->eat;
                send(sock, &health, sizeof(float), 0);
                send(sock, &noise, sizeof(float), 0);
                send(sock, &eat, sizeof(float), 0);
                creature->died = false;
            }

            // receive from python
            for(Creature *creature: grid->allCreatures)
            {
                read(sock, buffer, sizeof(float) * 4);
                creature->legstatus[0] = buffer[0];
                creature->legstatus[1] = buffer[1];
                creature->bite         = buffer[2];
                creature->green_value  = static_cast<unsigned char>(buffer[3] * 255.f);
            }
        }
    }
}

void Render::renderCreature(Creature *creature, unsigned long x_cell, unsigned long y_cell)
{
    if(creature->vegan)
    {
        spCreatureBody.setColor(sf::Color(0, creature->green_value, 255, 255));
    }
    else
    {
        spCreatureBody.setColor(sf::Color(255, creature->green_value, 0, 255));
    }

    spCreatureBody.setScale(creature->size / 163.f, creature->size / 163.f);
    spCreatureLeftFoot.setScale(creature->size / 163.f, creature->size / 163.f);
    spCreatureRightFoot.setScale(creature->size / 163.f, creature->size / 163.f);
    spCreatureMouth.setScale(creature->size / 163.f, creature->size / 163.f);

    if(creature->eat == 1.f)
    {
        unsigned char mouthcolor = static_cast<unsigned char>(creature->bite * 155.f);
        spCreatureMouth.setColor(sf::Color(mouthcolor + 100, 100, 100, 255));
    }

    unsigned char legcolor = static_cast<unsigned char>(creature->legstatus[0] * 255.f);
    spCreatureLeftFoot.setColor(sf::Color(legcolor, legcolor, legcolor, 255));
    legcolor = static_cast<unsigned char>(creature->legstatus[1] * 255.f);
    spCreatureRightFoot.setColor(sf::Color(legcolor, legcolor, legcolor, 255));

    spCreatureBody.setPosition(sf::Vector2f(float(x_cell) * CELLSIZE + creature->position[0],
                                            float(y_cell) * CELLSIZE + creature->position[1]));
    spCreatureLeftFoot.setPosition(sf::Vector2f(float(x_cell) * CELLSIZE + creature->position[0],
                                                float(y_cell) * CELLSIZE + creature->position[1]));
    spCreatureRightFoot.setPosition(sf::Vector2f(float(x_cell) * CELLSIZE + creature->position[0],
                                                 float(y_cell) * CELLSIZE + creature->position[1]));
    spCreatureMouth.setPosition(sf::Vector2f(float(x_cell) * CELLSIZE + creature->position[0],
                                             float(y_cell) * CELLSIZE + creature->position[1]));

    spCreatureBody.setRotation(Utils::rad2deg(creature->rotation));
    spCreatureLeftFoot.setRotation(Utils::rad2deg(creature->rotation));
    spCreatureRightFoot.setRotation(Utils::rad2deg(creature->rotation));
    spCreatureMouth.setRotation(Utils::rad2deg(creature->rotation));

    window.draw(spCreatureBody);
    window.draw(spCreatureLeftFoot);
    window.draw(spCreatureRightFoot);
    window.draw(spCreatureMouth);
}

void Render::renderTree(Tree *tree, unsigned long x_cell, unsigned long y_cell)
{
    spTree.setPosition(
        sf::Vector2f(float(x_cell) * CELLSIZE + tree->position[0], float(y_cell) * CELLSIZE + tree->position[1]));
    spTree.setRotation(Utils::rad2deg(tree->rotation));

    window.draw(spTree);

    for(int i = 0; i < 8; i++)
    {
        if(tree->fruits[i].status == 1.f)
        {
            spFruit.setPosition(float(x_cell) * CELLSIZE + tree->fruits[i].position[0],
                                float(y_cell) * CELLSIZE + tree->fruits[i].position[1]);
            window.draw(spFruit);
        }
    }
}

// Argument: port
int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        std::cerr << "Usage: ./render [port]" << std::endl;
        exit(EXIT_FAILURE);
    }
    srand(static_cast<unsigned int>(time(nullptr)));
    int port       = std::atoi(argv[1]);
    Render *render = new Render(port);
    render->startLoop();
    return 0;
}
