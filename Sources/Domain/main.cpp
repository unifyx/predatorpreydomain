#include "main.h"

Main::Main(int port): parameters(Parameters::getInstance())
{
    currentFrame = 0;
    establishConnection(static_cast<unsigned short>(port));
    receiveParameters();
    grid = new Grid();
}

void Main::receiveParameters()
{
    read(sock, &parameters.framesBetweenControl, sizeof(int));
    read(sock, &parameters.windowName, sizeof(char) * 64);
    parameters.windowName[64] = '\0';

    read(sock, &parameters.numTrees, sizeof(int));
    read(sock, &parameters.numPredators, sizeof(int));
    read(sock, &parameters.numPreys, sizeof(int));

    int worldsize_temp;
    read(sock, &worldsize_temp, sizeof(int));
    parameters.worldsize = CELLSIZE * float(worldsize_temp);
    read(sock, &parameters.growCoolDown, sizeof(float));
    parameters.growCoolDown *= 60.f;

    read(sock, &parameters.velocityDamp, sizeof(float));
    read(sock, &parameters.rotationDamp, sizeof(float));

    read(sock, &parameters.sightRange, sizeof(float));
    read(sock, &parameters.sightAngle, sizeof(float));
    read(sock, &parameters.numReceptors, sizeof(int));

    read(sock, &parameters.biteCoolDown, sizeof(float));
    parameters.biteCoolDown *= 60.f;
    read(sock, &parameters.lifeTime, sizeof(float));
    parameters.lifeTime *= 60.f;

    read(sock, &parameters.predatorWeight, sizeof(float));
    read(sock, &parameters.predatorBiteForce, sizeof(float));
    read(sock, &parameters.predatorMeatGain, sizeof(float));
    read(sock, &parameters.predatorFruitGain, sizeof(float));
    read(sock, &parameters.predatorLegPower, sizeof(float));
    read(sock, &parameters.predatorRotPower, sizeof(float));

    read(sock, &parameters.preyWeight, sizeof(float));
    read(sock, &parameters.preyBiteForce, sizeof(float));
    read(sock, &parameters.preyMeatGain, sizeof(float));
    read(sock, &parameters.preyFruitGain, sizeof(float));
    read(sock, &parameters.preyLegPower, sizeof(float));
    read(sock, &parameters.preyRotPower, sizeof(float));
}

void Main::establishConnection(unsigned short port)
{
    sock = 0;
    struct sockaddr_in serv_addr;
    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        std::cerr << "Socket creation error" << std::endl;
        exit(EXIT_FAILURE);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port   = htons(port);

    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        std::cerr << "Invalid address/ Address not supported" << std::endl;
        exit(EXIT_FAILURE);
    }

    if(connect(sock, reinterpret_cast<struct sockaddr *>(&serv_addr), sizeof(serv_addr)) < 0)
    {
        std::cerr << "Connection Failed" << std::endl;
        exit(EXIT_FAILURE);
    }
}

[[noreturn]] void Main::startLoop()
{
    // Start the game loop
    for(;;)
    {
        currentFrame++;

        grid->updatePhysics();
        if(currentFrame % static_cast<unsigned long>(parameters.framesBetweenControl) == 0)
        {
            grid->updateInputs();
            // send to python

            for(Creature *creature: grid->allCreatures)
            {
                const char padding = 0;
                send(sock, &(creature->died), sizeof(creature->died), 0);
                send(sock, &padding, sizeof(padding), 0);
                send(sock, &padding, sizeof(padding), 0);
                send(sock, &padding, sizeof(padding), 0);
                for(int i = 0; i < parameters.numReceptors * 3; i++)
                {
                    float value = float(creature->left_eye[i]) / 255.f;
                    send(sock, &value, sizeof(float), 0);
                }
                for(int i = 0; i < parameters.numReceptors * 3; i++)
                {
                    float value = float(creature->right_eye[i]) / 255.f;
                    send(sock, &value, sizeof(float), 0);
                }
                float health = creature->health;
                float noise  = creature->noise;
                float eat    = creature->eat;
                send(sock, &health, sizeof(float), 0);
                send(sock, &noise, sizeof(float), 0);
                send(sock, &eat, sizeof(float), 0);
                creature->died = false;
            }

            // receive from python
            for(Creature *creature: grid->allCreatures)
            {
                read(sock, buffer, sizeof(float) * 4);
                creature->legstatus[0] = buffer[0];
                creature->legstatus[1] = buffer[1];
                creature->bite         = buffer[2];
                creature->green_value  = static_cast<unsigned char>(buffer[3] * 255.f);
            }
        }
    }
}

// Argument: port
int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        std::cerr << "Usage: ./main [port]" << std::endl;
        exit(EXIT_FAILURE);
    }
    srand(static_cast<unsigned int>(time(nullptr)));
    int port    = std::atoi(argv[1]);
    Main *world = new Main(port);
    world->startLoop();
}
