#pragma once

#include "creature.h"
#include "tree.h"
#include "utils.h"

#include <bits/stdc++.h>
#include <iostream>

#define CELLSIZE 256.f
#define COLLISIONFORCE 0.05f
#define TREECOLFORCE 0.3f
#define FRUITCOLFORCE 0.2f

class GridCell
{
  public:
    void insert(Creature *);
    void insert(Tree *);
    void remove(Creature *);
    void remove(Tree *);
    void removeCreature(int);
    void removeTree(int);

    GridCell();

    std::vector<Creature *> creatures;
    std::vector<Tree *> trees;

  private:
};

class Grid
{
  public:
    std::vector<Creature *> allCreatures;
    std::vector<GridCell> cells;
    unsigned long numCells;

    void insert(Creature *);
    void insert(Tree *);
    void updatePhysics();
    void updateInputs();

    Grid();

  private:
    void calculateCollisions();
    void refreshEyes();
    void refreshPositions();
    void refreshAffiliations();
    void refreshNoise();
    void attachToSHM();

    int cellSightRange;
    int cellCollisionRange;
    float collisionRange;

    Parameters &parameters;
};
