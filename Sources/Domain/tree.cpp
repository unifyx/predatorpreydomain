#include "tree.h"

Fruit::Fruit()
{
}

Tree::Tree(float *_pos, float _rotation): parameters(Parameters::getInstance())
{
    // set position and rotation
    position[0] = _pos[0];
    position[1] = _pos[1];

    rotation = _rotation;

    // set the fruit positions
    for(int i = 0; i < 8; i++)
    {
        float angle           = 2.f * float(M_PI) * float(i) / 8.f;
        fruits[i]             = Fruit();
        fruits[i].position[0] = position[0] + TREESIZE * 0.58f * cosf(angle + rotation);
        fruits[i].position[1] = position[1] + TREESIZE * 0.58f * sinf(angle + rotation);
        fruits[i].status      = 1.f;
    }
}

void Tree::update()
{
    for(int i = 0; i < 8; i++)
    {
        fruits[i].status = std::min(1.f, fruits[i].status + (1.f / parameters.growCoolDown));
    }
}
