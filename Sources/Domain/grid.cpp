#include "grid.h"

GridCell::GridCell()
{
    creatures = std::vector<Creature *>();
    trees     = std::vector<Tree *>();
}

void GridCell::insert(Creature *creature)
{
    creatures.push_back(creature);
}

void GridCell::insert(Tree *tree)
{
    trees.push_back(tree);
}

void GridCell::removeCreature(int index)
{
    creatures.erase(creatures.begin() + index);
}

void GridCell::removeTree(int index)
{
    trees.erase(trees.begin() + index);
}

Grid::Grid(): parameters(Parameters::getInstance())
{
    numCells           = static_cast<unsigned long>(parameters.worldsize / CELLSIZE);
    collisionRange     = (TREESIZE + FRUITSIZE) / 2;
    cellSightRange     = int(std::ceil((parameters.sightRange + (TREESIZE + FRUITSIZE) / 2) / CELLSIZE));
    cellCollisionRange = int(std::ceil(collisionRange / CELLSIZE));

    cells.reserve(numCells * numCells);
    allCreatures.reserve(static_cast<unsigned long>(parameters.numPredators + parameters.numPreys));
    for(unsigned long i = 0; i < numCells * numCells; i++)
    {
        cells.push_back(GridCell());
    }

    for(int n = 0; n < parameters.numTrees; n++)
    {
        unsigned long cell_x;
        unsigned long cell_y;
        float pos_x;
        float pos_y;

        cell_x = static_cast<unsigned long>(rand()) % numCells;
        cell_y = static_cast<unsigned long>(rand()) % numCells;
        pos_x  = Utils::randf(CELLSIZE);
        pos_y  = Utils::randf(CELLSIZE);

        for(int attempts = 0; attempts < 10000; attempts++)
        {
            cell_x = static_cast<unsigned long>(rand()) % numCells;
            cell_y = static_cast<unsigned long>(rand()) % numCells;
            pos_x  = Utils::randf(CELLSIZE);
            pos_y  = Utils::randf(CELLSIZE);

            bool collided = false;

            for(int i = -cellCollisionRange * 2; i <= cellCollisionRange * 2; i++)
            {
                for(int j = -cellCollisionRange * 2; j <= cellCollisionRange * 2; j++)
                {
                    GridCell *otherCell =
                        &cells[static_cast<unsigned long>(int(cell_x + numCells) + i) % numCells * numCells +
                               (static_cast<unsigned long>(int(cell_y + numCells) + j) % numCells)];
                    for(unsigned long m = 0; m < otherCell->trees.size(); m++)
                    {
                        Tree *otherTree = otherCell->trees[m];
                        float rel_x     = pos_x - otherTree->position[0] - float(i) * CELLSIZE;
                        float rel_y     = pos_y - otherTree->position[1] - float(j) * CELLSIZE;

                        float distance = sqrtf(powf(rel_x, 2) + powf(rel_y, 2));
                        if(distance < 1.3f * (TREESIZE + FRUITSIZE))
                        {
                            collided = true;
                        }
                    }
                }
            }
            if(!collided)
            {
                break;
            }
        }
        cells[cell_x * numCells + cell_y].trees.push_back(
            new Tree(new float[2]{pos_x, pos_y}, Utils::randf(2.f * M_PIf32)));
    }
    for(int i = 0; i < parameters.numPredators; i++)
    {
        Creature *newCreature = new Creature(false, Utils::randf(2.f * M_PIf32),
                                             new float[2]{Utils::randf(CELLSIZE), Utils::randf(CELLSIZE)});
        cells[static_cast<unsigned long>(rand()) % (numCells * numCells)].creatures.push_back(newCreature);
        allCreatures.push_back(newCreature);
    }
    for(int i = 0; i < parameters.numPreys; i++)
    {
        Creature *newCreature = new Creature(true, Utils::randf(2.f * M_PIf32),
                                             new float[2]{Utils::randf(CELLSIZE), Utils::randf(CELLSIZE)});
        cells[static_cast<unsigned long>(rand()) % (numCells * numCells)].creatures.push_back(newCreature);
        allCreatures.push_back(newCreature);
    }
}

void Grid::calculateCollisions()
{
    for(unsigned long x = 0; x < numCells; x++)
    {
        for(unsigned long y = 0; y < numCells; y++)
        {
            GridCell *curCell = &cells[x * numCells + y];
            for(unsigned long n = 0; n < curCell->creatures.size(); n++)
            {
                Creature *curCreature = curCell->creatures[n];
                for(int i = -cellCollisionRange; i <= cellCollisionRange; i++)
                {
                    for(int j = -cellCollisionRange; j <= 0; j++)
                    {
                        if(j == 0 && i > 0) continue;
                        GridCell *otherCell =
                            &cells[static_cast<unsigned long>(int(x + numCells) + i) % numCells * numCells +
                                   (static_cast<unsigned long>(int(y + numCells) + j) % numCells)];
                        for(unsigned long m = (i == 0 && j == 0) ? n + 1 : 0; m < otherCell->creatures.size(); m++)
                        {
                            Creature *otherCreature = otherCell->creatures[m];

                            float rel_x = curCreature->position[0] - otherCreature->position[0] - float(i) * CELLSIZE;
                            float rel_y = curCreature->position[1] - otherCreature->position[1] - float(j) * CELLSIZE;

                            // check if the same
                            float distance = sqrtf(powf(rel_x, 2) + powf(rel_y, 2));

                            if(distance < (curCreature->size + otherCreature->size) / 2)
                            {
                                float overlapForce =
                                    COLLISIONFORCE * (distance - (curCreature->size - otherCreature->size) / 2);
                                float x_force = overlapForce * rel_x / distance;
                                float y_force = overlapForce * rel_y / distance;

                                curCreature->vel[0] +=
                                    x_force * curCreature->size / (curCreature->size + otherCreature->size);
                                curCreature->vel[1] +=
                                    y_force * curCreature->size / (curCreature->size + otherCreature->size);
                                otherCreature->vel[0] -=
                                    x_force * otherCreature->size / (curCreature->size + otherCreature->size);
                                otherCreature->vel[1] -=
                                    y_force * otherCreature->size / (curCreature->size + otherCreature->size);
                            }
                        }
                    }
                    for(int j = -cellCollisionRange; j <= cellCollisionRange; j++)
                    {
                        GridCell *otherCell =
                            &cells[static_cast<unsigned long>(int(x + numCells) + i) % numCells * numCells +
                                   (static_cast<unsigned long>(int(y + numCells) + j) % numCells)];
                        for(unsigned long m = 0; m < otherCell->trees.size(); m++)
                        {
                            Tree *curTree = otherCell->trees[m];
                            float rel_x   = curCreature->position[0] - curTree->position[0] - float(i) * CELLSIZE;
                            float rel_y   = curCreature->position[1] - curTree->position[1] - float(j) * CELLSIZE;

                            float distance = sqrtf(powf(rel_x, 2) + powf(rel_y, 2));
                            // check if colliding
                            if(distance < (curCreature->size + TREESIZE) / 2)
                            {
                                // do elastic dynamic-dynamic collission
                                float overlapForce = TREECOLFORCE * (distance - (curCreature->size - TREESIZE) / 2);
                                curCreature->vel[0] +=
                                    -0.2f * curCreature->vel[0] + overlapForce * rel_x / distance / curCreature->size;
                                curCreature->vel[1] +=
                                    -0.2f * curCreature->vel[1] + overlapForce * rel_y / distance / curCreature->size;
                            }
                            for(int o = 0; o < 8; o++)
                            {
                                Fruit curFruit = curTree->fruits[o];
                                if(curFruit.status != 1.f)
                                {
                                    continue;
                                }
                                float frel_x    = curCreature->position[0] - curFruit.position[0] - float(i) * CELLSIZE;
                                float frel_y    = curCreature->position[1] - curFruit.position[1] - float(j) * CELLSIZE;
                                float fdistance = sqrtf(powf(frel_x, 2) + powf(frel_y, 2));
                                // check if colliding
                                if(fdistance < (curCreature->size + FRUITSIZE) / 2)
                                {
                                    // do elastic dynamic-dynamic collission
                                    float overlapForce =
                                        FRUITCOLFORCE * (fdistance - (curCreature->size - FRUITSIZE) / 2);
                                    curCreature->vel[0] += overlapForce * frel_x / fdistance / curCreature->size;
                                    curCreature->vel[1] += overlapForce * frel_y / fdistance / curCreature->size;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
void Grid::refreshPositions()
{
    for(unsigned long x = 0; x < numCells; x++)
    {
        for(unsigned long y = 0; y < numCells; y++)
        {
            GridCell *curCell = &cells[x * numCells + y];
            for(unsigned long i = 0; i < curCell->creatures.size(); i++)
            {
                // set the new velocity
                Creature *curCreature = curCell->creatures[i];

                curCreature->update();
                curCreature->health = std::min(1.f, curCreature->health);

                if(curCreature->health > 0.f)
                {
                    curCreature->position[0] += curCreature->vel[0];
                    curCreature->position[1] += curCreature->vel[1];
                }
                else
                {
                    curCreature->position[0] = Utils::randf(parameters.worldsize);
                    curCreature->position[1] = Utils::randf(parameters.worldsize);
                    curCreature->health      = .5f;
                    curCreature->died        = true;
                }
            }
            for(unsigned long i = 0; i < curCell->trees.size(); i++)
            {
                curCell->trees[i]->update();
            }
        }
    }
}

void Grid::refreshAffiliations()
{
    for(unsigned long x = 0; x < numCells; x++)
    {
        for(unsigned long y = 0; y < numCells; y++)
        {
            GridCell *curCell = &cells[x * numCells + y];
            for(unsigned long i = 0; i < curCell->creatures.size(); i++)
            {
                // set the new velocity
                Creature *curCreature  = curCell->creatures[i];
                unsigned long target_x = x;
                unsigned long target_y = y;
                if(curCreature->position[0] > 0.f)
                {
                    target_x =
                        static_cast<unsigned long>(int(x + numCells) + int(curCreature->position[0] / CELLSIZE)) %
                        numCells;
                }
                else
                {
                    target_x =
                        static_cast<unsigned long>(int(x + numCells) - int(curCreature->position[0] / CELLSIZE) - 1) %
                        numCells;
                }
                if(curCreature->position[1] > 0.f)
                {
                    target_y =
                        static_cast<unsigned long>(int(y + numCells) + int(curCreature->position[1] / CELLSIZE)) %
                        numCells;
                }
                else
                {
                    target_y =
                        static_cast<unsigned long>(int(y + numCells) - int(curCreature->position[1] / CELLSIZE) - 1) %
                        numCells;
                }

                if(target_x != x || target_y != y)
                {
                    curCell->creatures.erase(curCell->creatures.begin() + int(i));
                    cells[target_x * numCells + target_y].creatures.push_back(curCreature);
                    curCreature->position[0] = std::fmod(curCreature->position[0] + CELLSIZE, CELLSIZE);
                    curCreature->position[1] = std::fmod(curCreature->position[1] + CELLSIZE, CELLSIZE);
                    i--;
                }
            }
        }
    }
}
void Grid::refreshNoise()
{
    for(unsigned long x = 0; x < numCells; x++)
    {
        for(unsigned long y = 0; y < numCells; y++)
        {
            GridCell *curCell = &cells[x * numCells + y];
            for(unsigned long p = 0; p < curCell->creatures.size(); p++)
            {
                Creature *curCreature = curCell->creatures[p];
                curCreature->noise    = 0.f;
                for(int i = -cellSightRange; i < cellSightRange; i++)
                {
                    for(int j = -cellSightRange; j < cellSightRange; j++)
                    {
                        GridCell *otherCell =
                            &cells[static_cast<unsigned long>(int(x + numCells) + i) % numCells * numCells +
                                   (static_cast<unsigned long>(int(y + numCells) + j) % numCells)];
                        for(unsigned long n = 0; n < otherCell->creatures.size(); n++)
                        {
                            Creature *otherCreature = otherCell->creatures[n];
                            float dis               = sqrtf(
                                powf(curCreature->position[0] - otherCreature->position[0] - float(i) * CELLSIZE, 2) +
                                powf(curCreature->position[1] - otherCreature->position[1] - float(j) * CELLSIZE, 2));

                            curCreature->noise = std::min(
                                1.f, curCreature->noise +
                                         std::max(parameters.sightRange - dis, 0.f) / parameters.sightRange *
                                             sqrtf(powf(otherCreature->vel[0], 2) + powf(otherCreature->vel[1], 2)) /
                                             curCreature->maxspeed);
                        }
                    }
                }
            }
        }
    }
}

void Grid::refreshEyes()
{
    for(unsigned long x = 0; x < numCells; x++)
    {
        for(unsigned long y = 0; y < numCells; y++)
        {
            GridCell *curCell = &cells[x * numCells + y];
            for(unsigned long p = 0; p < curCell->creatures.size(); p++)
            {
                Creature *curCreature = curCell->creatures[p];

                float rel_m_x = -cosf(curCreature->rotation - M_PI_2f32) * (curCreature->size / 2 + MOUTHRANGE);
                float rel_m_y = -sinf(curCreature->rotation - M_PI_2f32) * (curCreature->size / 2 + MOUTHRANGE);
                float e       = -1.f;
                for(;;)
                {
                    //-1.f => Left
                    float eye_x = -cosf(curCreature->rotation - M_PI_2f32 + e * M_PI_4f32) * curCreature->size / 2;
                    float eye_y = -sinf(curCreature->rotation - M_PI_2f32 + e * M_PI_4f32) * curCreature->size / 2;
                    for(int m = 0; m < parameters.numReceptors; m++)
                    {
                        unsigned char redHit   = 0;
                        unsigned char greenHit = 0;
                        unsigned char blueHit  = 0;
                        float disHit           = parameters.sightRange;

                        float ray = curCreature->rotation - M_PI_2f32 + e * M_PI_4f32 +
                                    e * parameters.sightAngle * float(m - parameters.numReceptors / 2) /
                                        static_cast<float>(parameters.numReceptors);

                        float ray_x = -cosf(ray);
                        float ray_y = -sinf(ray);

                        for(int i = -cellSightRange; i < cellSightRange; i++)
                        {
                            for(int j = -cellSightRange; j < cellSightRange; j++)
                            {
                                GridCell *otherCell =
                                    &cells[static_cast<unsigned long>(int(x + numCells) + i) % numCells * numCells +
                                           (static_cast<unsigned long>(int(y + numCells) + j) % numCells)];
                                for(unsigned long n = 0; n < otherCell->creatures.size(); n++)
                                {
                                    Creature *otherCreature = otherCell->creatures[n];
                                    if(curCreature == otherCreature)
                                    {
                                        continue;
                                    }

                                    float rel_x =
                                        curCreature->position[0] - otherCreature->position[0] - float(i) * CELLSIZE;
                                    float rel_y =
                                        curCreature->position[1] - otherCreature->position[1] - float(j) * CELLSIZE;
                                    if(curCreature->eat == 1.f &&
                                       sqrtf(powf(rel_x - rel_m_x, 2) + powf(rel_y - rel_m_y, 2)) <
                                           otherCreature->size / 2)
                                    {
                                        curCreature->eat -= curCreature->bite;
                                        otherCreature->health -= curCreature->biteforce * curCreature->bite;
                                        curCreature->health += curCreature->meatFoodGain * curCreature->bite;
                                    }
                                    float distance = parameters.sightRange;
                                    if(Utils::getDistance(&distance, rel_x, rel_y, ray_x, ray_y, eye_x, eye_y,
                                                          otherCreature->size / 2))
                                    {
                                        if(distance < disHit)
                                        {
                                            disHit         = distance;
                                            float discount = (parameters.sightRange - distance) / parameters.sightRange;
                                            if(otherCreature->vegan)
                                            {
                                                blueHit = static_cast<unsigned char>(discount * 255.f);
                                                redHit  = 0;
                                            }
                                            else
                                            {
                                                redHit  = static_cast<unsigned char>(discount * 255.f);
                                                blueHit = 0;
                                            }
                                            greenHit = static_cast<unsigned char>(discount *
                                                                                  float(otherCreature->green_value));
                                        }
                                    }
                                }
                                for(unsigned long n = 0; n < otherCell->trees.size(); n++)
                                {
                                    Tree *curTree = otherCell->trees[n];
                                    float rel_x = curCreature->position[0] - curTree->position[0] - float(i) * CELLSIZE;
                                    float rel_y = curCreature->position[1] - curTree->position[1] - float(j) * CELLSIZE;

                                    float tDistance = parameters.sightRange;
                                    if(Utils::getDistance(&tDistance, rel_x, rel_y, ray_x, ray_y, eye_x, eye_y,
                                                          TREESIZE / 2))
                                    {
                                        if(tDistance < disHit)
                                        {
                                            disHit = tDistance;
                                            float discount =
                                                (parameters.sightRange - tDistance) / parameters.sightRange;
                                            redHit   = static_cast<unsigned char>(discount * float(TREE_R));
                                            greenHit = static_cast<unsigned char>(discount * float(TREE_G));
                                            blueHit  = static_cast<unsigned char>(discount * float(TREE_B));
                                        }
                                    }

                                    for(int o = 0; o < 8; o++)
                                    {
                                        Fruit *curFruit = &(curTree->fruits[o]);
                                        if(curFruit->status != 1.f)
                                        {
                                            continue;
                                        }
                                        float frel_x =
                                            curCreature->position[0] - curFruit->position[0] - float(i) * CELLSIZE;
                                        float frel_y =
                                            curCreature->position[1] - curFruit->position[1] - float(j) * CELLSIZE;

                                        if(curCreature->eat == 1.f &&
                                           sqrtf(powf(frel_x - rel_m_x, 2) + powf(frel_y - rel_m_y, 2)) < FRUITSIZE / 2)
                                        {
                                            curCreature->eat -= curCreature->bite;
                                            curCreature->health += curCreature->fruitFoodGain * curCreature->bite;
                                            curFruit->status -= curCreature->bite;
                                        }
                                        float fDistance;
                                        if(Utils::getDistance(&fDistance, frel_x, frel_y, ray_x, ray_y, eye_x, eye_y,
                                                              FRUITSIZE / 2))
                                        {
                                            if(fDistance < disHit)
                                            {
                                                disHit = fDistance;
                                                float discount =
                                                    (parameters.sightRange - fDistance) / parameters.sightRange;
                                                redHit   = static_cast<unsigned char>(discount * float(FRUIT_R));
                                                greenHit = static_cast<unsigned char>(discount * float(FRUIT_G));
                                                blueHit  = static_cast<unsigned char>(discount * float(FRUIT_B));
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if(e == -1.f)
                        {
                            curCreature->left_eye[3 * (parameters.numReceptors - 1 - m)]     = redHit;
                            curCreature->left_eye[3 * (parameters.numReceptors - 1 - m) + 1] = greenHit;
                            curCreature->left_eye[3 * (parameters.numReceptors - 1 - m) + 2] = blueHit;
                        }
                        else
                        {
                            curCreature->right_eye[3 * m]     = redHit;
                            curCreature->right_eye[3 * m + 1] = greenHit;
                            curCreature->right_eye[3 * m + 2] = blueHit;
                        }
                    }
                    if(e == 1.f) break;
                    e = 1.f;
                }
            }
        }
    }
}

void Grid::updatePhysics()
{
    calculateCollisions();
    refreshPositions();
    refreshAffiliations();
}

void Grid::updateInputs()
{
    refreshNoise();
    refreshEyes();
}
