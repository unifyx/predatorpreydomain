#pragma once

#include "parameters.h"

#include <bits/stdc++.h>
#include <cmath>
#include <math.h>

#define MOUTHRANGE 12.f

class Creature
{
  public:
    bool vegan;
    float size;

    // status
    float position[2];
    float rotation;
    float vel[2];
    float rotvel;

    // inputs
    float eat;
    unsigned char *left_eye;
    unsigned char *right_eye;
    float health;
    float noise;  // number of agents nearby, velocity a factor
    bool died;

    // outputs
    float legstatus[2];
    float bite;
    unsigned char green_value;

    float biteforce;
    float meatFoodGain;
    float fruitFoodGain;
    float legpower;
    float rotpower;
    float maxspeed;

    Creature(bool _vegan, float _rotation, float *position);
    void update();

  private:
    Parameters &parameters;
};
