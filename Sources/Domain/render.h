#include "grid.h"

#include <SFML/Graphics.hpp>
#include <SFML/System/Vector2.hpp>
#include <arpa/inet.h>
#include <cmath>
#include <iostream>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

class Render
{
  public:
    void startLoop();

    Render(int);

  private:
    sf::Texture texCreatureBody;
    sf::Texture texCreatureLeftFoot;
    sf::Texture texCreatureRightFoot;
    sf::Texture texCreatureMouth;
    sf::Texture texTree;
    sf::Texture texFruit;

    sf::Sprite spCreatureBody;
    sf::Sprite spCreatureLeftFoot;
    sf::Sprite spCreatureRightFoot;
    sf::Sprite spCreatureMouth;
    sf::Sprite spTree;
    sf::Sprite spFruit;

    sf::RenderWindow window;
    sf::Clock sf_clock;
    sf::Color background = sf::Color(50, 50, 50, 255);

    void initRenderWindow(unsigned int, unsigned int);
    void initResources();
    void renderCreature(Creature *, unsigned long, unsigned long);
    void renderTree(Tree *, unsigned long, unsigned long);
    void zoom(float, int, int);
    void establishConnection(unsigned short);

    sf::View view;

    unsigned long currentFrame;
    Creature *selectedCreature;
    Grid *grid;
    int sock;
    float currentZoom;
    float buffer[sizeof(float) * 4];

    void receiveParameters();
    Parameters &parameters;
};
