\chapter{The Domain}
\section{Implementation}
To ensure high performance I decided to do the implementation of the domain in C++. C would have been faster, but the abstractions C++ offers facilitate the programming process and leads to a cleaner and thus more maintainable result.
The final program is then controlled with sockets, i.e. by sending and receiving the necessary information over the loopback interface.
\section{World}
The world itself is two-dimensional and torus-shaped. Torus-shaped in this context means that once an agent goes outside the boundaries of the world at any side (up, right, down, left) it reappears on the opposing side, e.g. an agent that leaves the world on the right side emerges on the left.
\subsection{Grid}
To put the torus shape into practice regardless of the size of the world, the easiest approach was to use a grid composed of individual cells. Each one of these cells contains all objects, agents, etc. within its boundaries. This procedure also offers the advantage of locality. Events in the world normally influence objects that are located near them. Instead of taking the whole world into account, only the relevant entities are included in the calculations.
\subsection{Agents}
An agent in this domain is an actor with a mouth, legs and a base color (either red or blue). Each agent has a well defined position, rotation, velocity and rotational velocity. It can influence these parameters using two outputs: The degree of activation of the left leg and of the right leg. When activating the left leg, the forward motion and rotational velocity to the right are increased. On the other hand, the reverse happens when activating the right leg: the forward motion and rotational velocity to the left grow. To summarize: if the agents wants to go to the right it has to activate the left leg, to go to the left it has to use the right leg and to go straight it has to activate both. \\
\begin{figure}[h]
   \centering
   \includegraphics[width=.4\textwidth]{Figures/anatomy}
 \caption{The anatomy of an agent}
 \label{fig:Anatomy}
\end{figure}
Additionally, there are two more action an agent can perform: It can bite, resulting in increased health when there is something in front of its mouth. If a bite was successfully performed a cool down is started until it can bite again to impede biting multiple times a second. Furthermore, an agent can change its green value. A high green value of an agent with the base color red would result in a yellow agent as it is perceived by other agents. The analogue principle is true for a blue agent that would be perceived as cyan. This feature is intended to make the learning of communication possible.\\
Of course the agent also needs some inputs about its surroundings. To this end the agent has two eyes and one ``ear''. The degree of activation of the ear corresponds to the amount of movement around the agent, including its own, clipped to be smaller than one. The eyes, one on each side of the creature, furthermore allow the agent to perceive a visual (meaning RGB) one-dimensional slice of the surrounding two-dimensional world. The perceived colors fade with the distance allowing for the implementation of a maximum sight range.\\
All actions and inputs are continuous with values inside the interval between zero and one. \\
At each time step the health of the agent decreases by a small amount, should the health of the agent decrease to below zero, it ``dies'' and is respawned with half health to a new random position in the world. This can be avoided by eating.
\subsection{Food Sources}
An agent can increase its health by biting either other agents or by consuming fruit that is found around trees in the world.
Every tree has a fixed position and rotation that is randomly chosen at the start of the simulation. Around every tree there are eight fruits. If they are eaten, a countdown is started until they reappear again.
\begin{figure}[H]
    \centering
  \includegraphics[width=.4\textwidth]{Figures/tree.png}
  \caption{A tree missing a fruit}
  \label{fig:tree}
\end{figure}
\section{Physics}
To avoid the creatures reaching boundless speeds by continually accelerating a damping factor is applied at every time step. This is accomplished by multiplying the current velocity and rotational velocity with a factor between zero and one. A value of one would effectively disable dampening, while a value of zero would lead to a creature immediately coming to a halt after it stopped activating both legs.
Using the damping coefficient and the maximum power of the legs one can calculate maximum speed a creature can achieve: $$\text{maxSpeed} = \frac{\text{legPower} \cdot 2}{1.0 - \text{velocityDamp}}$$
In order to provide a richer environment with obstacles and rigid body physics all creatures can collide with each other, fruits and trees.
\section{Rendering}
In order to give the user ``something to look at'', the world has to be rendered. In this work SFML (Simple and Fast Multimedia Library) was used. It provides a high level interface for creating windows, managing OpenGL contexts, handling user input and more \cite{sfml}. With this library a user interface was implemented that renders all creatures, trees and fruit. \\
Furthermore, it is possible to use the mouse wheel to zoom into the simulation to see more details or to zoom out to get an overview. To get more information about the current status of a creature's input the user can click it. This shows an overlay that provides data about each visual receptor, the current perceived noise, the current bite cool down and the current health. \\
The appearance of the creature itself changes depending on the current actions it performs. An activated leg appears white instead of black when activated, the color changes depending on the green value and the mouth is red when attempting to bite.
\begin{figure}[H]
    \centering
  \includegraphics[width=.55\textwidth]{Figures/creature.png}
    \caption{An agent activating its right leg and trying to bite, the input overlay shows that the left eye perceives the tree while the left sees the other agents}
  \label{fig:tree}
\end{figure}
\section{Optimizations}
As previously mentioned a focus of this work was to provide a high performance simulation. After activating compiler optimizations, such as fast-math, further more manual optimizations where implemented.
\subsection{Nearest Neighbor Search}
When checking for entities that are relevant for computing interactions it is not necessary to iterate over all other entities in the whole domain. A big help for avoiding this problem was the implementation of a grid. This allows to only check the cells which are in range (e.g. sight range). Of course now if one creature is in the range of the other the reciprocal is also true. A naive nested loop would perform double the needed checks. By using a pattern as in Figure \ref{fig:Cells} it is possible to only perform half the checks while not skipping any potential entity. The same logic is applied to all entities in the cell the creature resides in (blue in the figure).
\begin{figure}[h]
   \centering
   \includegraphics[width=.8\textwidth]{Figures/cells}
 \caption{The pattern how cells are checked for potential entities}
 \label{fig:Cells}
\end{figure}
\subsection{Vision}
Upon closer inspection one could notice that all entities are practically composed of circles. This facilitates the computation of vision significantly. From each eye so-called rays propagate and depending on which entity they hit (if any) they perceive a different color. \\
\begin{figure}[h]
   \centering
   \includegraphics[width=\textwidth]{Figures/vision}
 \caption{Typical setup for calculating intersections between a line and a circle}
 \label{fig:Vision}
\end{figure}
Using the formula
\begin{equation}
(e_x + d_x \lambda - t_x) ^ 2 + (e_y + d_y \lambda - t_y) ^ 2 = r ^ 2
\end{equation}
\\ and solving for $\lambda$ leads to two results
\begin{multline}
    $$\lambda = (\pm \frac{1}{2} \sqrt{(2 d_x e_x - 2 d_x t_x + 2 e_y d_y - 2 d_y t_y)^2 - 4 (d_x^2 + d_y^2)} \cdot \\ \overline{(e_x^2 - 2 e_x t_x + e_y^2 - 2 e_y t_y + t_y^2 - r^2 + t_x^2)} - \\ d_x e_x + d_x t_x - e_y d_y + d_y t_y)/(d_x^2 + d_y^2)$$
\end{multline}

The smallest calculated positive distance determines the entity that is hit first and so decides the resulting color of this ray.
\subsection{Collision Detection}
The prevalence of circles also makes calculating collisions between entities easier since simple elastic collisions between spherical objects are one of the simplest to compute. This is due to the resulting forces are parallel to the connecting line between both center points.
\begin{figure}[h]
   \centering
   \includegraphics[width=.5\textwidth]{Figures/collision}
 \caption{Forces in a collision between two spherical objects}
 \label{fig:Collision}
\end{figure}
\section{Parametrization}
A major focus of this domain was to make it as customizable as possible to be employed in many different use cases.
The changeable parameters are as follows:

\begin{itemize}
\item \textbf{framesBetweenControl}: simulation steps performed by the domain between exchange of data over sockets
\item \textbf{windowName}: window name of the domain when not running in headless mode
\item \textbf{numTrees}: total number of trees
\item \textbf{numPredators}: total number of predators
\item \textbf{numPreys}: total number of preys
\item \textbf{worldsize}: world size in units of vertical and horizontal number of cells
\item \textbf{growCoolDown}: how much time [s] a fruit takes to fully regenerate
\item \textbf{velocityDamp}: at every simulation step the current velocity of the creature is multiplied with this factor
\item \textbf{rotationDamp}: at every simulation step the current rotational velocity of the creature is multiplied with this factor
\item \textbf{sightRange}: maximum range at which the creature's eyes perceive the environment
\item \textbf{sightAngle}: the angle at which the creature's eyes perceive the environment
\item \textbf{numReceptors}: total number of sensors per eye, each receptor consists of three inputs (R+G+B)
\item \textbf{biteCoolDown}: how long [s] a creature has to wait between bites
\item \textbf{lifeTime}: maximum length of time [s] a creature lives if it has full health and stops eating
\item \textbf{predatorWeight}: how much a predator weighs, relevant for collisions and in conjunction with leg power for acceleration
\item \textbf{predatorBiteForce}: maximum amount of health that is deducted from the target if a bite of the predator connects
\item \textbf{predatorMeatGain}: maximum amount of health a predator gains from biting another creature
\item \textbf{predatorFruitGain}: maximum amount of health a predator gains from biting fruit
\item \textbf{predatorLegPower}: maximum amount of velocity gain per simulation step a predator gains when activating one leg 
\item \textbf{predatorRotPower}: maximum amount of rotational velocity gain per simulation step a predator gains when activating one leg
\item \textbf{preyWeight}: how much a prey weighs, relevant for collisions and in conjunction with leg power for acceleration
\item \textbf{preyBiteForce}: maximum amount of health that is deducted from the target if a bite of the prey connects
\item \textbf{preyMeatGain}: maximum amount of health a prey gains from biting another creature
\item \textbf{preyFruitGain}: maximum amount of health a prey gains from biting fruit
\item \textbf{preyLegPower}: maximum amount of velocity gain per simulation step a prey gains when activating one leg
\item \textbf{preyRotPower}: maximum amount of rotational velocity gain per simulation step a prey gains when activating one leg
\end{itemize}
\section{Headless Operation}
A user interface is a great idea to provide a visualization and better understanding for the user. However, when using the simulation on remote computers without a GUI or when learning it is not necessary. Disabling it removes the limitation of waiting between frames and reduces the amount of calculations, providing greater speeds. \\
To this end the domain compiles to two different binaries, one with rendering and the other for headless operation. Using different binaries allow applying different optimizations to each one, resulting in further performance gains.
\section{Python Interface}
Nowadays almost all machine learning frameworks use python with a C back end. This means it is indispensable to provide an interface with Python as it cannot be expected from the programmer to do machine learning in C++. For that I implemented an API that handles all control of the domain including starting and stopping it, setting all domain parameters, receiving all inputs from all creatures and sending actions to each agent. When setting these parameters they are checked if they conform to certain conditions (such as being between zero and one) and if not a detailed exception is raised. After starting the domain all parameters are locked. \\
To visualize the simplicity of the interface I included a short example:
\begin{lstlisting}[float=H,caption=Exemplary usage of the API,label=lst:hw,language=Python, numbers=left, basicstyle=\small, numberstyle=\tiny, stepnumber=2, numbersep=8pt, escapeinside={//@}{@//},xleftmargin=3ex,xrightmargin=1ex]
#Create domain
domain = PredatorPreyDomain()

#Set parameters
domain.numReceptors = 16
domain.headless     = True

#Start domain
domain.start()

#Loop
for _ in range(numSteps):
    #Receive inputs from domain
    domain.step()
    #Control logic
    domain.predators[0].leftLeg = 0.8
    ...
    #Send outputs to agents/domain
    domain.cont()
\end{lstlisting}
\section{Performance}
In order to measure performance initial domain parameters were used:
\begin{itemize}
\item Predators: 4
\item Preys: 8
\item Trees: 3
\item Receptors: 16
\item Sight-range: 512
\end{itemize}

For each experiment one of these parameters was changed to find out its impact.
\begin{figure}[H]
\centering
\includegraphics[width=.8\textwidth]{Figures/number_of_trees}
 \caption{Number of trees}
 \label{fig:Trees}
\end{figure}
The number of trees only minimally influences the performance as they are static objects that do not require a lot of computation.
\begin{figure}[H]
   \centering
   \includegraphics[width=.8\textwidth]{Figures/sightrange}
 \caption{Sight range}
 \label{fig:Sightrange}
\end{figure}
The same is true for the sight range, even though more cells have to be checked, this does not have a large effect in a sparse environment.
\begin{figure}[H]
   \centering
   \includegraphics[width=.8\textwidth]{Figures/number_of_preys}
 \caption{Number of preys}
 \label{fig:Preys}
\end{figure}
However when the user increases the number of agents in the simulation, the performance degrades visibly because the number of calculations increases quadratically.
\begin{figure}[H]
   \centering
   \includegraphics[width=.8\textwidth]{Figures/number_of_receptors}
 \caption{Number of receptors}
 \label{fig:Receptors}
\end{figure}
If the number of receptors is increased, the number of calculations increases linearly, however, because more data has to be sent over the sockets, the performance impact is more noticeable.
\begin{figure}[H]
   \centering
   \includegraphics[width=.8\textwidth]{Figures/worldsize}
 \caption{World size}
 \label{fig:Worldsize}
\end{figure}
With an increase of the world size, the domain becomes more sparse, which leads to fewer entities per cell, and thus to fewer checks.
\vspace{1em}
\textit{For all measurements a Intel(R) Core(TM) i3-7100U CPU @ 2.40GHz was used.}
\section{Learnability}
One very important goal after programming the domain is showing that it is learnable. To prove that I ran 10 simulations of a creature learning a behavior using Upside Down Reinforcement Learning (UDRL) \cite{udrl} and recorded the time it lived before it died. 
\begin{figure}[H]
   \centering
   \includegraphics[width=.8\textwidth]{Figures/learn}
 \caption{Typical learning progress made an agent, after the last generation in the graph it did not die anymore for over 47000 time steps}
 \label{fig:Learn}
\end{figure}
As one can easily see, after two real-time hours (simulated in 10 minutes) the agent learned to navigate the environment and significantly prolonged the expected live time.
\section{Related Work}
\subsection{Pommerman}
Pommerman is a multi-agent environment based on the classic game Bomberman. In contrast to this work it does not include a (visual) sensor input and data is exchanged in raw form, including a communication channel. Additionally all outputs are discrete and the world has a fixed size, which moves the whole focus of the domain to learning high level strategies instead of low level control \cite{pommerman}.
\subsection{OpenSpiel}
OpenSpiel is a framework for creating algorithms and games for machine-learning purposes. It includes over 20 games of many different scenarios such as matrix games, table top games, board games etc. Instead of using sockets to communicate with the API, the C++ program exposes python bindings \cite{openspiel}. All the presented domains use discrete controls and have their focus on developing strategies and/or performant search algorithms.
\subsection{Star Craft II}
Star Craft II is a real time strategy game from Blizzard Entertainment which involves controlling hundreds of units while making high level decisions about e.g. in-game economics. A new prestigious project by Deep Mind was to learn to control this game using self-play and imitation learning to surpass even the best human players. At each time step there are $10^{26}$ actions to choose from, which makes this domain very challenging for a machine-learning algorithm to master. Because of emerging complexity, it features much higher strategic depth in comparison to this work \cite{sc2}.
\section{Future Work}
\subsection{Team Play}
Until now it is not possible to reward agents differently depending on the type (or color) of creature they bite. Allowing for this would allow implementing teams, where ``friendly fire'' is discouraged as there would be no benefit for the individual agent.
\subsection{Performance}
While testing the domain, the bottleneck of the simulation always was the learning process, so that further speedup of the domain was not necessary. Should this become a concern however, there are many additional improvements which could be implemented:
\begin{itemize}
\item Parallelization of the simulation on multiple cores. This should not be too difficult, as the world is already subdivided into grid cells, that can be processed rather independently.
\item Parallelization of the simulation on multiple computers. Should the need for enormous worlds arise, distributing the computations on multiple different machines, combined with message passing, might be necessary.
\item Implicit single-core parallelization. Using different data structures in conjunction with proper memory alignment to improve data access patterns, compute to memory access ratio and vectorization potential the performance could be enhanced by a huge factor.
\end{itemize}
